drop table if exists ip_config;

create table ip_config (
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   ip VARCHAR(64) NOT NULL UNIQUE,
   hostFqdn VARCHAR(128) NOT NULL,
   active INTEGER NOT NULL,
   CHECK(active>=0 AND active <=1)
);


insert into ip_config (ip, hostFqdn, active) values('127.0.0.1','mta-prod-1', 1);
insert into ip_config (ip, hostFqdn, active) values('127.0.0.2','mta-prod-1', 0);
insert into ip_config (ip, hostFqdn, active) values('127.0.0.3','mta-prod-2', 1);
insert into ip_config (ip, hostFqdn, active) values('127.0.0.4','mta-prod-2', 1);
insert into ip_config (ip, hostFqdn, active) values('127.0.0.5','mta-prod-2', 0);
insert into ip_config (ip, hostFqdn, active) values('127.0.0.6','mta-prod-3', 0);

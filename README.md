MTA Hosting Optimizer
=====================

Short Description
-----------------
As an operations engineer I want to create MTA Hosting Optimization service that uncovers the inefficient servers hosting only few active MTAs.

Requirements
------------
* Python3
* SQLite

Quick Start
-----------
1. `pip install virtualenv`
2. `virtualenv “name of virtual environment”`
3. Now go to your terminal. Go to the directory that contains the file called activate. The file called activate is found inside a folder called bin for OS X and Linux.
4. `name of virtual environmnet/bin/activate`
5. `pip install -r requirments.txt`
6. `python app.py`
7. Point to your borwser http://127.0.0.1:5000/host/ And you can set your dynamic parameter like http://127.0.0.1:5000/host/?threshold=3

Dockerization
-------------
* Docker file provided in the repository `Dockerfile`

```FROM ubuntu:18.10
RUN apt-get update
RUN apt-get install -y python3 python3-dev python3-pip
COPY ./ ./app
WORKDIR ./app
RUN pip3 install -r requirements.txt
ENTRYPOINT [ "python3" ]
CMD [ "app.py" ]
```
GitLab Build Pipeline
---------------------
* Build pipeline file has been given in the repository `.gitlab-ci.yml`
```
stages:
  - test
  - build

test:
  stage: test
  image: python:3.7-slim
  script:
   - pip3 install -r requirements.txt
   - python3 app-test.py

build:
  stage: build
  image: docker:latest

  services:
    - docker:dind

  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2

  before_script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com

  script:
    - docker build -t registry.gitlab.com/arifurreza/mta_hosting_optimizer .
    - docker push registry.gitlab.com/arifurreza/mta_hosting_optimizer

```

Rancher Deployment
------------------
1. Haproxy Loadbalancer exist
2. Two instances of the application are running at the same time.
3. Only one service container of the application must run per host.
4. `docker-compose.yaml​` file describing general service deployment ​and `rancher-compose.yaml​` file describing rancher specific service deployment both are the exists in the repository.
import json
import sqlite3
from contextlib import closing
from flask import Flask, request, g, jsonify


# Configuration
DATABASE = 'mta.db'


# APP
app = Flask(__name__)
app.config.from_object(__name__)


def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('mta.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


# ROUTING
@app.route("/")
def welcome():
    response = {'Hello': 'World'}
    return jsonify(response)


# ********** GET Unoptimized Host List ************ #
@app.route("/host/", methods=['GET'])
def host():
    threshold = 1

    try:
        x = int(request.args.get('threshold'))
        if x > 0:
            threshold = x
    except Exception as e:
        # print(e)
        pass

    query = "select hostFqdn from ip_config where active=1 group by hostFqdn having count(active) <={0}"
    query += " union"
    query += " select hostFqdn from ip_config group by hostFqdn having count(active) <={0}"
    query = query.format(threshold)

    data = {
        "threshold": threshold,
        "hosts": []
    }
    for row in query_db(query):
        data['hosts'].append(row[0])

    return jsonify(data)


if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')



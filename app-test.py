from app import app

import unittest
import os

class BasicTestCase(unittest.TestCase):

    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'{"Hello":"World"}\n')

    def test_database(self):
        tester = os.path.exists("mta.db")
        self.assertTrue(tester)

    def test_host(self):
        tester = app.test_client(self)
        response = tester.get('/host/', content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'{"hosts":["mta-prod-1","mta-prod-3"],"threshold":1}\n')

    def test_host_threshold_1(self):
        tester = app.test_client(self)
        response = tester.get('/host/?threshold=1', content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'{"hosts":["mta-prod-1","mta-prod-3"],"threshold":1}\n')

    def test_host_threshold_2(self):
        tester = app.test_client(self)
        response = tester.get('/host/?threshold=2', content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'{"hosts":["mta-prod-1","mta-prod-2","mta-prod-3"],"threshold":2}\n')

if __name__ == '__main__':
    unittest.main()